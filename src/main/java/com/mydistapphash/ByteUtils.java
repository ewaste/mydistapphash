package com.mydistapphash;

public class ByteUtils {
    public static int bytesToInt(byte[] bytes) {
        int intSize = Integer.SIZE/8;
        int number = 0;
        for(int i=0; i<intSize; i++) {
            number = number | ((int)bytes[i] >>> i*8);
        }
        return number;
    }

    public static byte[] intToBytes(int number) {
        int intSize = Integer.SIZE/8;
        byte[] bytes = new byte[Integer.SIZE/8];
        for(int i=0; i<intSize; i++) {
            bytes[i] = (byte) ((number >>> i*8) & 0xff);
        }
        return bytes;
    }

    public static boolean bytesToBoolean(byte[] bytes) {
        return !(bytes[0] == 0);
    }

}
