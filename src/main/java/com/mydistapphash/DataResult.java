package com.mydistapphash;

import java.nio.ByteBuffer;

public class DataResult {
    byte[] result;
    private byte[] nonce;
    private boolean isOk;
    public DataResult(byte[] result) {
        this.result = result;
    }

    public boolean isOk(){
        if(result[0] == (byte)1){
            return true;
        } else {
            return false;
        }
    }

    public byte[] getNonce(){
        return ByteBuffer.allocate(result.length-1).put(result,1,result.length-1).array();
    }

}
