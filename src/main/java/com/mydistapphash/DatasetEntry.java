package com.mydistapphash;


import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class DatasetEntry {
    int count;
    long start;
    long end;
    byte[] bytes;
    public DatasetEntry(int count, long start, long end, byte[] bytes) {
        this.count = count;
        this.start = start;
        this.end = end;
        this.bytes = bytes;
    }

    public byte[] getOutput() {
        return ByteBuffer.allocate(20+bytes.length).putInt(count).putLong(start).putLong(end).put(bytes).array();
    }


}
