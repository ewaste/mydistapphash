package com.mydistapphash;

import com.ewaste.server.Server;
import com.ewaste.server.IServerCallbacks;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

class ServerCallbacks implements IServerCallbacks<DatasetEntry, DataResult> {
    public void gatherResult(DatasetEntry input, DataResult result) {
        System.out.println("Is hash ok? " + result.isOk());
    }

    @Override
    public DataResult decodeResult(byte[] bytes) {
        return new DataResult(bytes);
    }

    @Override
    public byte[] encodeInput(DatasetEntry datasetEntry) {
        return datasetEntry.getOutput();
    }

}

public class Entry {
    public static void main(String[] args) {
        Server server = null;
        SimpleKernel kernel = new SimpleKernel();
        ConcurrentLinkedQueue<DatasetEntry> dataset = new ConcurrentLinkedQueue<>();
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        dataset.add(RandomDataGenerator.getRandomOutput());
        ServerCallbacks serverCallbacks = new ServerCallbacks();
        try {
            server = new Server(19955,
                    System.getenv("ANDROID_SDK")+"/build-tools/28.0.3/dx.bat",
                    kernel,
                    dataset,
                    serverCallbacks, 4);
            server.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }
}
