package com.mydistapphash;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomDataGenerator {
    public static DatasetEntry getRandomOutput(){
        Random rng = new Random(new Date().getTime());
        int count = ThreadLocalRandom.current().nextInt(1,15);
        long start = ThreadLocalRandom.current().nextLong(1,9999999);
        long end = ThreadLocalRandom.current().nextLong(start,9999999);
        byte[] bytes = new byte[20];
        rng.nextBytes(bytes);
        return new DatasetEntry(count,start,end,bytes);
    }
}
