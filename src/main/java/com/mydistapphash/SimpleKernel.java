package com.mydistapphash;

import com.ewaste.kernel.IKernel;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class SimpleKernel implements IKernel {
    public byte[] execute(byte[] input) {
        byte[] output = new byte[1];
        output[0] = 1;
        MessageDigest crypt = null;
        try {
            crypt = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        int count = ByteBuffer.wrap(input).getInt(0);
        long start = ByteBuffer.wrap(input).getLong(4);
        long end = ByteBuffer.wrap(input).getLong(12);
        byte[] bytes = new byte[input.length-20];
        for(int i=0; i<input.length-20; i++){
            bytes[i] = input[i+20];
        }
        byte[] mask = new byte[20];
        for (int j = 0; j < 20; j++) {
            mask[j] = (byte) ((j<count/8)?0:0xff);
        }

        int isOk = 0;
        long i;
        for (i = start; i < end; i++) {
            byte[] nonce = ByteBuffer.allocate(8).putLong(i).array();
            byte[] bytes1 = ByteBuffer.allocate(input.length-12).put(bytes).put(nonce).array();
            crypt.reset();
            crypt.update(bytes1);
            byte[] sha1 = crypt.digest();
            for (int j = 0; j < count/8; j++) {
                if(sha1[j]==0){
                    isOk+=8;
                }
            }

            if((~sha1[count/8] & (byte) (-1<<(8-count%8))) == (byte) (-1<<(8-count%8))){
                isOk+=count%8;
            }

            if(isOk == count/8){
                System.out.println(i);
                return ByteBuffer.allocate(9).put((byte) 1).putLong(i).array();
            }
        }
        return ByteBuffer.allocate(1).put((byte) 0).array();
    }

}
